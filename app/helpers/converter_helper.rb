module ConverterHelper
   require 'ox'

  @@ins_range = {
  "Piccolo"             => {:low => 62, :high => 96},
  "Flute"               => {:low => 58, :high => 98},
  "Treble Flute"        => {:low => 67, :high => 103},
  "Recorder"            => {:low => 72, :high => 103},
  "Oboe"                => {:low => 58, :high => 93},
  "English Horn"        => {:low => 52, :high => 84},
  "B♭ Clarinet"         => {:low => 50, :high => 95},
  "A Clarinet"          => {:low => 48, :high => 93},
  "Bass Clarinet"       => {:low => 35, :high => 77},
  "Bassoon"             => {:low => 34, :high => 75},
  "Contrabassoon"       => {:low => 22, :high => 58},
  "Soprano Saxophone"   => {:low => 56, :high => 89},
  "Alto Saxophone"      => {:low => 49, :high => 83},
  "Tenor Saxophone"     => {:low => 44, :high => 77},
  "Baritone Saxophone"  => {:low => 37, :high => 71},
  "Bagpipe"             => {:low => 53, :high => 67},
  "Harmonica"           => {:low => 36, :high => 96},
  "Accordion"           => {:low => 35, :high => 93},
  "Horn in F"           => {:low => 35, :high => 77},
  "B♭ Cornet"           => {:low => 44, :high => 77},
  "C Trumpet"           => {:low => 54, :high => 86},
  "B♭ Trumpet"          => {:low => 53, :high => 77},
  "Trombone"            => {:low => 45, :high => 79},
  "Tenor Trombone"      => {:low => 35, :high => 81},
  "Bass Trombone"       => {:low => 34, :high => 70},
  "Euphonium"           => {:low => 34, :high => 70},
  "Tuba"                => {:low => 26, :high => 65},
  "B♭ Tuba"             => {:low => 28, :high => 72},
  "Timpani"             => {:low => 41, :high => 60},
  "Glockenspiel"        => {:low => 79, :high => 108},
  "Crotales"            => {:low => 84, :high => 108},
  "Vibraphone"          => {:low => 53, :high => 89},
  "Tubular Bells"       => {:low => 53, :high => 91},
  "Hand Bells"          => {:low => 24, :high => 120},
  "Xylophone"           => {:low => 65, :high => 108},
  "Marimba"             => {:low => 45, :high => 96},
  "Voice"               => {:low => 40, :high => 84},
  "Soprano"             => {:low => 60, :high => 84},
  "Mezzo-soprano"       => {:low => 57, :high => 81},
  "Alto"                => {:low => 52, :high => 77},
  "Tenor"               => {:low => 48, :high => 72},
  "Baritone"            => {:low => 43, :high => 67},
  "Bass"                => {:low => 40, :high => 64},
  "Piano"               => {:low => 12, :high => 108},
  "Harpsichord"         => {:low => 29, :high => 89},
  "Organ"               => {:low => 36, :high => 96},
  "Pipe Organ"          => {:low => 36, :high => 96},
  "Banjo"               => {:low => 48, :high => 84},
  "Classical Guitar"    => {:low => 40, :high => 88},
  "Acoustic Guitar"     => {:low => 40, :high => 88},
  "Electric Guitar"     => {:low => 40, :high => 88},
  "Harp"                => {:low => 24, :high => 104},
  "Ukulele"             => {:low => 60, :high => 84},
  "Bass Guitar"         => {:low => 24, :high => 67},
  "Acoustic Bass"       => {:low => 24, :high => 67},
  "Electric Bass"       => {:low => 24, :high => 67},
  "5-str. Electric Bass"=> {:low => 24, :high => 67},
  "Violin"              => {:low => 55, :high => 105},
  "Viola"               => {:low => 48, :high => 88},
  "Violoncello"         => {:low => 36, :high => 84},
  "Contrabass"          => {:low => 24, :high => 60},
  "Double Bass"         => {:low => 24, :high => 60}
  }

    @@score_hash = Hash.new
    @@score_hash[:ins_range] = @@ins_range
    
    def parse_file(attachment)
        Ox.load(attachment.read)
    end

    def find_parts(parsed)
        parts_array = []
        instrument_staff_array = []
        default_clef_array = []
        
        parsed.museScore.Score.locate("Part").each do |part|
            parts_array.push([part.trackName.text, part.locate("Staff").length])
            
            part.locate("Staff").each do |ins_staff|
                instrument_staff_array.push([ins_staff, part.trackName.text])

                if ins_staff.respond_to?("defaultClef")
                    default_clef_array.push(ins_staff.defaultClef.text)
                elsif !ins_staff.respond_to?("defaultClef")
                    default_clef_array.push("G")
                end
            end    
        end
        @@score_hash[:parts] = parts_array
        @@score_hash[:instrument_staff] = instrument_staff_array
        @@score_hash[:default_clef] = default_clef_array
        return @@score_hash
    end

    def find_notation_staves(parsed)
        notation_staff_array = []
        
        parsed.museScore.Score.locate("Staff").each do |notation_staff|
            notation_staff_array.push(notation_staff)
        end
        @@score_hash[:notation_staff] = notation_staff_array
        return @@score_hash
    end

    def get_measure_details(notation_staff_array)
        time_sig_array = []
        key_array = []
        clef_array = []
        pitch_array = []
        out_of_range_array = []
        chords_array = []
        measure_array = []
        
        notation_staff_array.each_with_index do |staff, index|
            staff.locate("Measure").each do |measure|
                if measure.respond_to?("TimeSig")
                    time_sig_array.push(["time", staff.id, measure.number, [measure.TimeSig.sigN.text, measure.TimeSig.sigD.text]])
                end
                if measure.respond_to?("KeySig")
                    key_array.push(["key", staff.id, measure.number, measure.KeySig.accidental.text])
                elsif !measure.respond_to?("KeySig") && (measure.number == "1")
                    key_array.push(["key", staff.id, measure.number, "C"])
                end
                if measure.respond_to?("Clef") && !(measure.number == "1" && measure.Clef.concertClefType.text == @@score_hash[:default_clef][index])
                    clef_array.push(["clef", staff.id, measure.number, measure.Clef.concertClefType.text])
                elsif !measure.respond_to?("Clef") && (measure.number == "1")
                    clef_array.push(["clef", staff.id, measure.number, @@score_hash[:default_clef][index]])
                end
                if measure.respond_to?("Chord")
                    measure.locate("Chord").each do |chord|
                        chord.locate("Note/pitch").each do |pitch|
                            pitch_array.push(pitch)
                            if ( pitch.text.to_i > @@ins_range[@@score_hash[:instrument_staff][index][1]][:high] ) || ( pitch.text.to_i < @@ins_range[@@score_hash[:instrument_staff][index][1]][:low] )
                                out_of_range_array.push(["chord", staff.id, measure.number, pitch.text])
                                if pitch.text.to_i > @@ins_range[@@score_hash[:instrument_staff][index][1]][:high] ? out_of_range_array.push("high") : out_of_range_array.push("low")
                                end
                            else
                                out_of_range_array.push(["chord", staff.id, measure.number, "In bounds"])
                            end
                        end
                    end
                end
                if measure.respond_to?("Chord") || measure.respond_to?("Rest")
                    chords_array.push(["notes&rests", staff.id, measure.number, chord_analyser(measure.nodes)])
                end
                measure_array.push(["measure", staff.id, measure.number, pitch_array])
            end
        end
        @@score_hash[:chords] = chords_array
        @@score_hash[:measures] = measure_array
        @@score_hash[:times] = time_sig_array
        @@score_hash[:ranges] = out_of_range_array
        @@score_hash[:keys] = key_array
        @@score_hash[:clefs] = clef_array
        return @@score_hash
    end

    def chord_analyser(nodes)
        chord_array = []
        nodes.each do |first|
            if first.value == "Chord" || first.value == "Rest"
                first.nodes.each do |second|
                    second.nodes.each do |third|
                        if !third.is_a? String
                            if third.value == "pitch"
                                third.nodes.each do |fourth|
                                    chord_array.push(num_to_notation(fourth.to_i))
                                end
                            end
                        else chord_array.push(third)
                        end
                    end
                end
                if first.value == "Rest"
                    chord_array.push(first.value)
                end
            end
        end
        return chord_array
    end

    def analyse(attachment)
        parsed = parse_file(attachment)
        find_parts(parsed)
        find_notation_staves(parsed)
        get_measure_details(@@score_hash[:notation_staff])
        prepare_view(@@score_hash)
    end

    def num_to_notation (num)
        note =
        if num == 11 || num % 12 == 11
            "B" + (num / 12).to_s
        elsif num == 10 || num % 12 == 10
            "A#/B♭" + (num / 12).to_s
        elsif num == 9 || num % 12 == 9
            "A" + (num / 12).to_s
        elsif num == 8 || num % 12 == 8
            "G#/A♭" + (num / 12).to_s
        elsif num == 7 || num % 12 == 7
            "G" + (num / 12).to_s
        elsif num == 6 || num % 12 == 6
            "F#/G♭" + (num / 12).to_s
        elsif num == 5 || num % 12 == 5
            "F" + (num / 12).to_s
        elsif num == 4 || num % 12 == 4
            "E" + (num / 12).to_s
        elsif num == 3 || num % 12 == 3
            "D#/E♭" + (num / 12).to_s
        elsif num == 2 || num % 12 == 2
            "D" + (num / 12).to_s
        elsif num == 1 || num % 12 == 1
            "C#/D♭" + (num / 12).to_s
        elsif num == 0 || num % 12 == 0
            "C" + (num / 12).to_s
        end
        return note
    end
    
    def prepare_view(analysed)
        table_array = []
        row_array = []
        temp_array = []
        convention = []
        
        analysed[:parts].each do |part_name, number|
            analysed[:instrument_staff].each do |staff, name|
                row_array.push([name, ["convention", num_to_notation(analysed[:ins_range][name][:low].to_i), num_to_notation(analysed[:ins_range][name][:high].to_i)]])
                [analysed[:keys].select{|w, x, y, z| x == staff.id}.length,
                analysed[:times].select{|w, x, y, z| x == staff.id}.length,
                analysed[:clefs].select{|w, x, y, z| x == staff.id}.length,
                analysed[:ranges].select{|w, x, y, z| (x == staff.id) && (z != "In bounds")}.length,
                analysed[:chords].select{|w, x, y, z| x == staff.id}.length].max.times do |i|
                    row_array.push([name, analysed[:clefs].select{|w, x, y, z| x == staff.id}[i],
                                   analysed[:times].select{|w, x, y, z| x == staff.id}[i],
                                   analysed[:keys].select{|w, x, y, z| x == staff.id}[i],
                                   analysed[:ranges].select{|w, x, y, z| (x == staff.id) && (z != "In bounds")}[i],
                                   analysed[:chords].select{|w, x, y, z| x == staff.id}[i]])
                end
            end
            temp_array = Marshal.dump(row_array)
            loaded = Marshal.load(temp_array)
            table_array.push(part_name, loaded, convention)
            row_array.clear
        end
        return table_array
    end    
    
end