class Score < ApplicationRecord
    belongs_to :user
    mount_uploader :attachment, ScoreUploader

end