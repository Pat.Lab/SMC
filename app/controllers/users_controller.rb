class UsersController < ApplicationController

    before_action :authenticate_user!, if: :login

    def show
      @user = current_user
    end
    
    def edit
    end

    def update
      respond_to do |format|
        if @user.update(user_params)
          sign_in(@user == current_user ? @user : current_user, :bypass => true)
          format.html { redirect_to @user, notice: 'Your profile was successfully updated.' }
          format.json { head :no_content }
        else
          format.html { render action: 'edit' }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
    
    def scores
      @user = current_user
      @scores = Score.where('user_id' => @user.id).find_each
    end
    
    def download_score
      @score = Score.find(params[:id])
      if @score
        send_file(@score.attachment.path, type: 'application/octet-stream', disposition: 'attachment')
      end
    end
    
    def finish_signup
      @user = current_user
      if request.patch? && params[:user] && params[:user][:email]
        if @user.update(user_params)
          # @user.skip_reconfirmation!
          sign_in(@user, :bypass => true)
          redirect_to @user, notice: 'Your profile was successfully updated.'
        else
          @show_errors = true
        end
      end
    end
    
    def destroy
      @user.destroy
      respond_to do |format|
        format.html { redirect_to root_url }
        format.json { head :no_content }
      end
    end
    
    private
    
    def set_user
      @user = User.find(params[:id])
    end
    
    def user_params
      accessible = [ :name, :email, :first_name ]
      accessible << [ :password, :password_confirmation ] unless params[:user][:password].blank?
      params.require(:user).permit(accessible)
    end
    
    def login
      warden = env['warden']
        !(warden.authenticated?(:admin))
    end
   
end