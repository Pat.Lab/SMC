class ScoresController < ApplicationController
    include ConverterHelper

    before_action :authenticate_user!, if: :login, :except => [:index]

    def index
        @user = current_user
        @scores = Score.where('share' => true, 'copied' => false).find_each 
    end

    def show
        @score = Score.find(params[:id])
        if (user_signed_in? && (current_user.id == @score.user_id)) || !(:login)
            @parsed = JSON.parse(@score.score_hash)
            render 'show'
        elsif (user_signed_in? && (current_user.id != @score.user_id))
            redirect_to current_user
        end
    end

    def create
        @score = Score.new(score_params)
        @score.user_id = current_user.id

        if @score.save
            redirect_to users_scores_path, notice: "The score #{@score.name} has been uploaded."
        else
            redirect_to users_scores_path, notice: "Not saved"
        end
    end
    
    def destroy
        @user = current_user
        @score = Score.find(params[:id])
        @score.destroy
        redirect_to users_scores_path, notice: "The score #{@score.name} has been deleted."
    end

    def share
        @score = Score.find(params[:id])
        if @score.update(share: true)
            redirect_to users_scores_path
        end
    end
    
    def unshare
        @score = Score.find(params[:id])
        if @score.update(share: false)
            redirect_to users_scores_path
        end
    end
    
    def copy
        @score1 = Score.find(params[:id])
        @score = Score.new
        @score.user_id = current_user.id
        @score.name = @score1.name
        @score.attachment = @score1.attachment
        @score.uploaded_by = @score1.uploaded_by
        @score.copied = true
        
        if @score.save
            redirect_to users_scores_path
        end
    end
        
    def convert
        @score = Score.find(params[:id])
        if (user_signed_in? && (current_user.id == @score.user_id)) || !(:login)
            @new_hash = analyse(@score.attachment)
            @json_hash = JSON.generate(@new_hash)
            @score.update(score_hash: @json_hash)
            redirect_to @score
        elsif (user_signed_in? && (current_user.id != @score.user_id))
            redirect_to current_user
        end
    end
        
    private
    
    def score_params
      params.require(:score).permit(:name, :attachment, :share, :uploaded_by, :copied)
    end

    def login
      warden = env['warden']
        !(warden.authenticated?(:admin))
    end

end