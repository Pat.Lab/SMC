# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
 Rails.application.config.assets.precompile += %w( bootstrap.min.css )
 Rails.application.config.assets.precompile += %w( bootstrap3.min.css )
 Rails.application.config.assets.precompile += %w( bootstrap.min.js )
 Rails.application.config.assets.precompile += %w( bootstrap3.min.js )
 Rails.application.config.assets.precompile += %w( bootstrap-social )
 Rails.application.config.assets.precompile += %w( justified-nav )
 Rails.application.config.assets.precompile += %w( jquery-3.1.1.slim.min )
 Rails.application.config.assets.precompile += %w( jquery.min )
 Rails.application.config.assets.precompile += %w( tether.min )
 Rails.application.config.assets.precompile += %w( ie10-viewport-bug-workaround )
 Rails.application.config.assets.precompile += %w( bootstrap-modal.min.css )
 Rails.application.config.assets.precompile += %w( bootstrap-modal-bs3patch.min )
 Rails.application.config.assets.precompile += %w( bootstrap-modal.min.js )
 Rails.application.config.assets.precompile += %w( bootstrap-modalmanager.min )
 Rails.application.config.assets.precompile += %w( signin )
 Rails.application.config.assets.precompile += %w( starter-template )
 Rails.application.config.assets.precompile += %w( font-awesome )