class AddShareToScores < ActiveRecord::Migration[5.0]
  def change
    add_column :scores, :share, :boolean
  end
end
