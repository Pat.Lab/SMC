class AddScoreHashToScores < ActiveRecord::Migration[5.0]
  def change
    add_column :scores, :score_hash, :string
  end
end
