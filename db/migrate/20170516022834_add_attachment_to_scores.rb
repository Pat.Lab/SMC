class AddAttachmentToScores < ActiveRecord::Migration[5.0]
  def change
    add_column :scores, :attachment, :string
  end
end
