class AddCopiedToScores < ActiveRecord::Migration[5.0]
  def change
    change_column :scores, :copied, :boolean, :default => false
  end
end
