class AddUploadedByToScores < ActiveRecord::Migration[5.0]
  def change
    add_column :scores, :uploaded_by, :string
  end
end
